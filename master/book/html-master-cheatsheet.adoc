= HTML Master Cheatsheet
:source-highlighter: rouge
:rouge-style: monokai
:toc:

include::../../includes/datalist/datalist.adoc[]

include::../../includes/details/details.adoc[]

include::../../includes/dialog/dialog.adoc[]

include::../../includes/img/img.adoc[]